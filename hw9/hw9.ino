
/**
 * sensorRecorder.h - will be abstracted after submission
 * Allows client definitions of pins[], values[], etc
 * Unexpected consequences will happen if these arrays are different sizes
 *  - Or if any of these sizes vary from .size
 */
#include <MsTimer2.h>

#define BAUD 57600
#define SAMPLERATE 20

struct sensorWriter {
  int size;
  volatile bool* isDigital;
  volatile int* values;
  volatile int* pins;
  volatile bool running = false;
  volatile bool readyToPrint = false;
} mySensorsWriter;

void sensorsAnalyzerFlash() {
  for (int i=0; i<mySensorsWriter.size; i++) {
    if (mySensorsWriter.isDigital) {
      mySensorsWriter.values[i] = digitalRead(mySensorsWriter.pins[i]);
    } else {
      mySensorsWriter.values[i] = analogRead(mySensorsWriter.pins[i]);
    }
  }
  mySensorsWriter.readyToPrint = true;
}

int configureSensors(int amount, int* pin, int* vals, bool* digitals) {
  mySensorsWriter.size = amount;
  mySensorsWriter.isDigital = digitals;
  mySensorsWriter.pins = pin;
  mySensorsWriter.values = vals;
  Serial.begin(BAUD);
  MsTimer2::set(SAMPLERATE, sensorsAnalyzerFlash);
}

void sensorsWriterPrint() {
  if (!mySensorsWriter.running) return;
  if (mySensorsWriter.readyToPrint) {
    if (mySensorsWriter.size > 0) {
      if (mySensorsWriter.isDigital[0]) {
        Serial.print(mySensorsWriter.values[0] ? "HIGH" : "LOW");
      } else {
        Serial.print(mySensorsWriter.values[0]);
      }
    }
    for (int i=1; i<mySensorsWriter.size; i++) {
      Serial.print(", ");
      if (mySensorsWriter.isDigital[i]) {
        Serial.print(mySensorsWriter.values[i] ? "HIGH" : "LOW");
      } else {
        Serial.print(mySensorsWriter.values[i]);
      }
    }
    Serial.println();
  }
}

void sensorsWriterCheck(String startS, String stopS) {
  if (Serial.available()) {
    String input = Serial.readStringUntil('\n');
    if (input == startS) {
      mySensorsWriter.running = true;
      MsTimer2::start();
    }
    if (input == stopS) {
      mySensorsWriter.running = false;
      MsTimer2::stop();
    }
  }
}

/**
 * End of sensorRecorder.h
 * Start of hw9.ino
 */

// #include "sensorRecorder.h"
#define STARTKEY "start"
#define STOPKEY "stop"
int pinSize = 3;
int pins[] = {3, 4, 5};
int values[] = {0, 0, 0};
bool digitals[] = {false, false, false};

void setup() {
  configureSensors(pinSize, pins, values, digitals);
}

void loop() {
  sensorsWriterCheck(STARTKEY, STOPKEY);
  sensorsWriterPrint();
}
